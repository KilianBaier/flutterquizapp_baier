import 'package:flutter/material.dart';
import 'package:quizapp/quiz.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyQuiz(title: "Quiz App"),
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),

    );
  }
}
