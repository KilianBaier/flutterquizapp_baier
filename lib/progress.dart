import 'package:flutter/material.dart';

class Progress extends StatelessWidget {
  final double progress;


  final double width;

  final double height;


  final Color? backgroundColor;

  const Progress({
    Key? key,
    required this.progress,
    this.width = 300,
    this.height = 50,
    this.backgroundColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Stack(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 6.0),
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 2),

                  color: backgroundColor,
                ),
              ),
            ),
            FractionallySizedBox(
              widthFactor: progress,
              child: Padding(
                padding: EdgeInsets.fromLTRB(10, 4, 10, 4),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.lightGreenAccent,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
