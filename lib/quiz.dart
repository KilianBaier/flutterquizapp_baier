import 'package:flutter/material.dart';
import 'package:quizapp/progress.dart';

class MyQuiz extends StatefulWidget {
  final String title;

  MyQuiz({super.key, required this.title});

  @override
  State<MyQuiz> createState() => _MyQuizState();
}

class _MyQuizState extends State<MyQuiz> {
  final Map<String, Map<String, bool>> data = {
    "Question One": {
      "Answer One": true,
      "Answer Two": false,
      "Answer Three": false,
      "Answer Four": false,
    },
    "Question Second": {
      "Answer One": true,
      "Answer Two": false,
      "Answer Three": false,
      "Answer Four": false,
    },
    "Question Three": {
      "Answer One": true,
      "Answer Two": false,
      "Answer Three": false,
      "Answer Four": false,
    },
  };

  int counter = 0;
  int right = 0;

  setIncreaseRight() {
    setState(() {
      right++;
    });
  }


  setIncreaseCounter() {
    setState(() {
      counter++;
    });
  }



  String getQuestion() {
    return data.keys.toList()[counter];
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Builder(
          builder: (context) {
            if (counter == data.length) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Right: $right"),
                  Text("Wrong: ${counter - right}"),
                  Progress(progress: right / counter),
                  ElevatedButton(
                    onPressed: () {
                      setState(() {
                        counter = 0;
                        right = 0;
                      });
                    },
                    child: Text("Restart"),
                  ),
                ],
              );
            }
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(getQuestion()),
                for (int i = 0; i < data[getQuestion()]!.length; i++)
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      onPressed: () {
                        if ([...data[getQuestion()]!.values][i]) {
                          print("Correct");
                          setIncreaseRight();
                        }
                        else {
                          print("Wrong");
                        }
                        setIncreaseCounter();
                      },
                      child: Text([...data[getQuestion()]!.keys][i]),
                    ),
                  ),
                Text("Right: $right"),
                Text("Wrong: ${counter - right}"),
              ],
            );
          }
        ),
      ),
    );
  }
}
